export default {
  "wst-title": "LuatOS Net Tester",
  "wst-llcom-text": "LLCOM",
  "wst-llcom-tip":
    "High degree of freedom serial debugging tool for running Lua scripts🛠。",
  "wst-connect-tcp": "Open TCP",
  "wst-connect-udp": "Open UDP",
  "wst-connect-tcp-ssl": "Open TCP-SSL",
  "wst-connect-close": "Disconnect",
  "wst-bug-tip": "Encounter any 🕷️problem🕷️, just feedback us 😺",
  "wst-bug-text": "Feedback",
  "wst-client-close-tip": "Click to disconnect the client",
  "wst-client-empty": "No connected device was detected",
  "wst-send-tip": "Message content",
  "wst-send-btn": "Send",
  "wst-use-hex-on-tip": "The message contents are encoded in hexadecimal",
  "wst-use-hex-off-tip": "The message contents are pure text",
  "wst-use-nl-on-tip":
    "Automatically append the newline character(\\r\\n) at the end of the message ",
  "wst-use-nl-off-tip": "Turn on to auto-append newline character(\\r\\n)",
  "wst-use-eb-on-tip": "AUTO-ECHO-BACK once receiving the client message",
  "wst-use-eb-off-tip": "Click to open AUTO-ECHO-BACK",
  "wst-show-hex-on-tip":
    "Show hex-encoded messages in log panel, which click to hide",
  "wst-show-hex-off-tip": "Turn on hex-encoded messages in log panel",
  "wst-log-asc-tip": "Sort log as ASC",
  "wst-log-desc-tip": "Sort log as DESC",
};
